package com.atlassian.bitbucket.linky.pipelines.yaml.vcs

import com.intellij.dvcs.repo.VcsRepositoryManager
import com.intellij.psi.PsiFile

fun repositoryForFile(file: PsiFile) =
    VcsRepositoryManager.getInstance(file.project)
        .getRepositoryForFile(file.virtualFile)
