package com.atlassian.bitbucket.linky.preferences

import com.atlassian.bitbucket.linky.logger
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.Task
import git4idea.commands.Git
import git4idea.repo.GitRepository
import java.util.concurrent.ConcurrentHashMap

class GitRepositoryPreferences : RepositoryPreferences {
    private val preferences = ConcurrentHashMap<String, GitConfigPreferences>()

    override fun preferences(repository: Repository): Preferences? =
        when (repository) {
            // Store under a constant path under .git directory
            is GitRepository -> preferences.computeIfAbsent(repository.repositoryFiles.shallowFile.absolutePath) {
                GitConfigPreferences(repository)
            }
            else -> null
        }
}

private class GitConfigPreferences(private val repository: GitRepository) : Preferences {
    private val log = logger()

    private val prefix = "bitbucket-linky."
    private val properties = ConcurrentHashMap<String, String>()

    init {
        object : Task.Backgroundable(repository.project, "Loading repository properties...", false) {
            override fun run(indicator: ProgressIndicator) {
                val result = Git.getInstance().config(repository, "--local", "--list")
                if (result.success()) {
                    result.output
                        .filter { it.startsWith(prefix) }
                        .map { it.removePrefix(prefix) }
                        .mapNotNull { it.parseKeyValue() }
                        .toMap(properties)
                } else {
                    throw IllegalStateException("Failed to load Git repository properties: ${result.errorOutputAsJoinedString}")
                }
            }
        }.queue()
    }

    override fun getProperty(key: String, defaultValue: String?): String? =
        properties[key] ?: defaultValue

    override fun setProperty(key: String, value: String) {
        properties[key] = value
        updateProperties(repository, "$prefix$key", value)
    }

    override fun removeProperty(key: String) {
        properties.remove(key)
        updateProperties(repository, "--unset", "$prefix$key")
    }

    private fun updateProperties(repository: GitRepository, vararg params: String) {
        object : Task.Backgroundable(repository.project, "Updating repository properties...", false) {
            override fun run(indicator: ProgressIndicator) {
                val result = Git.getInstance().config(repository, "--local", *params)
                if (!result.success()) {
                    log.error("Failed to update Git repository properties with parameters '${params.contentToString()}': ${result.errorOutputAsJoinedString}")
                }
            }
        }.queue()
    }
}
