package com.atlassian.bitbucket.linky.discovery

import com.intellij.dvcs.repo.VcsRepositoryMappingListener
import com.intellij.openapi.project.Project

class BitbucketRepositoriesMappingListener(private val project: Project) : VcsRepositoryMappingListener {
    override fun mappingChanged() {
        project.discoverRepositories()
    }
}
