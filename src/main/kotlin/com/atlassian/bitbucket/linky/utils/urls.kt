package com.atlassian.bitbucket.linky.utils

import org.apache.http.client.utils.URIBuilder

fun String.appendTrailSlash() = trimEnd('/') + '/'

fun URIBuilder.appendPathSegments(pathSegments: String): URIBuilder =
    appendPathSegments(pathSegments.split('/'))

fun URIBuilder.appendPathSegments(vararg pathSegments: String): URIBuilder =
    appendPathSegments(pathSegments.toList())

fun URIBuilder.appendPathSegments(pathSegments: List<String>): URIBuilder =
    setPathSegments(this.pathSegments + pathSegments)

fun URIBuilder.stripEmptyPathSegments(): URIBuilder =
    setPathSegments(this.pathSegments.filter { it.isNotEmpty() })
