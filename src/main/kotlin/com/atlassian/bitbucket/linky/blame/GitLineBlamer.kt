package com.atlassian.bitbucket.linky.blame

import com.atlassian.bitbucket.linky.LinkyFile
import com.atlassian.bitbucket.linky.RelativePath
import com.atlassian.bitbucket.linky.logger
import com.intellij.dvcs.repo.Repository
import git4idea.commands.Git
import git4idea.commands.GitCommand
import git4idea.commands.GitLineHandler
import git4idea.repo.GitRepository
import git4idea.util.StringScanner

private val log = logger()

class GitLineBlamerProvider : LineBlamerProvider {
    override fun getLineBlamer(repository: Repository): LineBlamer? = when (repository) {
        is GitRepository -> GitLineBlamer
        else -> null
    }
}

private object GitLineBlamer : LineBlamer {
    override fun blameLine(linkyFile: LinkyFile, lineNumber: Int): Pair<RelativePath, Int>? {
        val blameCommand = createBlameCommand(linkyFile, lineNumber)
        log.debug("Running command '$this'")
        val result = Git.getInstance().runCommand(blameCommand)

        return if (result.success()) {
            parseReferenceFromBlameOutput(result.outputAsJoinedString)
        } else {
            log.error("Error running command '$this': ${result.errorOutputAsJoinedString}")
            null
        }
    }

    private fun createBlameCommand(linkyFile: LinkyFile, lineNumber: Int): GitLineHandler {
        val handler = GitLineHandler(linkyFile.repository.project, linkyFile.repository.root, GitCommand.BLAME)
        handler.setStderrSuppressed(true)
        handler.charset = linkyFile.virtualFile.charset
        // TODO 2018.3 GitVcsApplicationSettings.getInstance().getAnnotateDetectMovementsOption() == INNER -> -M, == OUTER => -C, otherwise nothing
        // git blame -p -L51,+1 -n -w revision_number -- path/to/the/virtualFile.txt
        handler.addParameters("--porcelain", "-L$lineNumber,+1", "-n", "-w", linkyFile.revision)
        handler.endOptions()
        handler.addParameters(linkyFile.relativePath)
        return handler
    }

    private fun parseReferenceFromBlameOutput(output: String): Pair<RelativePath, Int>? {
        val s = StringScanner(output)
        s.spaceToken() // skip commit hash
        val lineNumber = s.spaceToken().toIntOrNull()

        if (lineNumber == null) {
            log.error("Failed to parse line number from git blame output '$output'")
            return null
        }

        s.nextLine()

        var filePath: String? = null
        while (s.hasMoreData() && !s.startsWith('\t')) {
            val key = s.spaceToken()
            val value = s.line()
            if (key == "filename") {
                filePath = value
                break
            }
        }

        return filePath?.let { Pair(it, lineNumber) }
    }
}
