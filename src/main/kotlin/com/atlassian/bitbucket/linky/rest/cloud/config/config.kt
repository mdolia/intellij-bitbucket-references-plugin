package com.atlassian.bitbucket.linky.rest.cloud.config

import com.atlassian.bitbucket.linky.rest.auth.OAuthConsumer
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud
import com.atlassian.bitbucket.linky.rest.cloud.registerPipelinesAdapters
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type
import java.net.URI
import java.time.ZonedDateTime

val BitbucketCloud.oAuthConsumer: OAuthConsumer
    get() = OAuthConsumer("WuCc3RmCsHYjWNfFzC", "S6DKhcxYHzPvNTuUkySnJUy7aCDNcW3y")

fun BitbucketCloud.oAuthAuthorizationUrl(stateToken: String): URI =
    baseUrl.resolve("/site/oauth2/authorize?client_id=${oAuthConsumer.id}&response_type=code&state=$stateToken")

fun defaultGson(): Gson = GsonBuilder()
    .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
    .registerTypeAdapter(
        ZonedDateTime::class.java,
        object : JsonDeserializer<ZonedDateTime> {
            override fun deserialize(json: JsonElement, type: Type, ctx: JsonDeserializationContext): ZonedDateTime {
                return ZonedDateTime.parse(json.asJsonPrimitive.asString)
            }
        }
    )
    .registerPipelinesAdapters()
    .create()
