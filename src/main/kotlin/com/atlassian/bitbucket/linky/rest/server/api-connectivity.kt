package com.atlassian.bitbucket.linky.rest.server

import com.atlassian.bitbucket.linky.rest.BitbucketRestException
import java.util.concurrent.CompletableFuture

interface ConnectivityTestApi {
    fun authenticatedResource(): CompletableFuture<Void>

    @Throws(BitbucketRedirectException::class)
    fun unauthenticatedResource(): CompletableFuture<Void>
}

class BitbucketRedirectException(val location: String) : BitbucketRestException("Redirect response from Bitbucket")
