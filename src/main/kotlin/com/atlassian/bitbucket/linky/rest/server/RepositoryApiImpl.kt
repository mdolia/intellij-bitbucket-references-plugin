package com.atlassian.bitbucket.linky.rest.server

import com.github.kittinunf.fuel.core.FuelManager
import com.google.gson.Gson

class RepositoryApiImpl(
    private val fuel: FuelManager,
    private val gson: Gson,
    private val instance: BitbucketServer,
    private val repositoryId: RepositoryId
) : RepositoryApi {
    override fun commit(commit: String): CommitApi = CommitApiImpl(fuel, gson, instance, repositoryId, commit)
}
