package com.atlassian.bitbucket.linky.rest.cloud

import com.atlassian.bitbucket.linky.rest.auth.Authentication
import com.atlassian.bitbucket.linky.rest.auth.Authentication.Guest
import com.atlassian.bitbucket.linky.rest.pagination.Page
import java.net.URI
import java.util.concurrent.CompletableFuture

interface BitbucketCloudApi {
    fun repository(repositoryId: RepositoryId): RepositoryApi
    fun snippets(workspaceId: String? = null): SnippetsApi
}

typealias CloudPage<T> = Page<BitbucketCloudApi, T>

interface RepositoryApi {
    fun commit(commit: String): CommitApi
    fun pipelines(): PipelinesApi
}

interface PipelinesApi {
    fun page(pageNumber: Int = 1): CompletableFuture<CloudPage<Pipeline>>
}

class BitbucketCloudApiConfig {
    var authentication: Authentication = Guest
}

const val BBC_MOCK_URL = "BitbucketLinky-BitbucketCloud-URL"

object BitbucketCloud {
    val baseUrl: URI by lazy {
        URI.create(System.getProperty(BBC_MOCK_URL) ?: "https://bitbucket.org/")
    }
    val apiBaseUrl: URI by lazy {
        URI.create(System.getProperty(BBC_MOCK_URL) ?: "https://api.bitbucket.org/")
    }
    // Kept for backward dependency, used to store state
    const val name = "Production"
}

data class RepositoryId(val workspace: String, val slug: String)
