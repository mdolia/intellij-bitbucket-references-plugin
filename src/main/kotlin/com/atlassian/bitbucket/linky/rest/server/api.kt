package com.atlassian.bitbucket.linky.rest.server

import com.atlassian.bitbucket.linky.rest.auth.Authentication
import com.atlassian.bitbucket.linky.rest.auth.Authentication.Guest
import com.atlassian.bitbucket.linky.rest.pagination.Page
import com.atlassian.bitbucket.linky.utils.appendTrailSlash
import java.net.URI

interface BitbucketServerApi {
    fun personalAccessTokens(username: String): PersonalAccessTokensApi
    fun repository(repositoryId: RepositoryId): RepositoryApi
    fun mirror(): MirrorApi
    fun testConnectivity(): ConnectivityTestApi
}

typealias ServerPage<T> = Page<BitbucketServerApi, T>

interface RepositoryApi {
    fun commit(commit: String): CommitApi
}

class BitbucketServerApiConfig {
    lateinit var instance: BitbucketServer
    var authentication: Authentication = Guest
}

data class BitbucketServer(val baseUrl: URI) {
    constructor(url: String) : this(URI.create(url.appendTrailSlash()))

    val hostname: String = baseUrl.host
    val apiBaseUrl: URI = baseUrl

    init {
        if (!baseUrl.path.endsWith('/')) {
            throw IllegalArgumentException("The URL path should end with a slash")
        }
    }
}

data class RepositoryId(val project: String, val slug: String)
