package com.atlassian.bitbucket.linky.rest.auth

@JvmInline
value class PersonalAccessToken(val value: String)
