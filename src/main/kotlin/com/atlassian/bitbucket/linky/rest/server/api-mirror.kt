package com.atlassian.bitbucket.linky.rest.server

import java.util.concurrent.CompletableFuture

interface MirrorApi {
    fun upstreamServer(): CompletableFuture<MirrorInfo>
}

data class MirrorInfo(
    val baseUrl: String,
    val state: String,
)
