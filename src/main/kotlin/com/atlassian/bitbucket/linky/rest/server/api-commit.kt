package com.atlassian.bitbucket.linky.rest.server

import com.atlassian.bitbucket.linky.rest.PullRequest
import java.util.concurrent.CompletableFuture

interface CommitApi {
    fun relatedPullRequests(start: Int = 0): CompletableFuture<ServerPage<PullRequest>>
}

enum class PullRequestState {
    MERGED, OPEN, DECLINED
}
