package com.atlassian.bitbucket.linky.rest.cloud.config

import com.atlassian.bitbucket.linky.generateServiceName
import com.atlassian.bitbucket.linky.rest.auth.OAuthTokens
import com.atlassian.bitbucket.linky.rest.auth.OAuthTokens.RefreshAndAccessTokens
import com.atlassian.bitbucket.linky.rest.auth.OAuthTokens.RefreshTokenOnly
import com.atlassian.bitbucket.linky.rest.auth.RefreshToken
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud
import com.intellij.credentialStore.CredentialAttributes
import com.intellij.credentialStore.Credentials
import com.intellij.ide.passwordSafe.PasswordSafe
import com.intellij.openapi.components.service
import java.util.concurrent.atomic.AtomicReference

interface OAuthTokensHolder {
    fun getOAuthTokens(): OAuthTokens?

    fun saveOAuthTokens(oAuthTokens: RefreshAndAccessTokens)
}

private const val TOKENS_DELIMITER = " : "

class DefaultOAuthTokensHolder : OAuthTokensHolder {

    private val tokens = AtomicReference<OAuthTokens?>()

    override fun getOAuthTokens(): OAuthTokens? {
        val maybeTokens = tokens.get()
        return maybeTokens ?: loadRefreshToken().also {
            tokens.compareAndSet(maybeTokens, it)
        }
    }

    override fun saveOAuthTokens(oAuthTokens: RefreshAndAccessTokens) {
        tokens.set(oAuthTokens)
        saveRefreshToken(oAuthTokens.refreshToken)
    }

    private fun loadRefreshToken(): RefreshTokenOnly? {
        val passwordSafe = service<PasswordSafe>()
        val tokenString = passwordSafe.getPassword(credentialsAttributes()) ?: return null
        val refreshToken = tokenString.substringBefore(TOKENS_DELIMITER)
        return RefreshTokenOnly(RefreshToken(refreshToken))
    }

    private fun saveRefreshToken(refreshToken: RefreshToken) {
        val passwordSafe = service<PasswordSafe>()
        passwordSafe.set(credentialsAttributes(), Credentials(BitbucketCloud.name, refreshToken.value))
    }

    private fun credentialsAttributes(): CredentialAttributes {
        val name = BitbucketCloud.name
        return CredentialAttributes(generateServiceName("Bitbucket Cloud $name"), name, null, false)
    }
}
