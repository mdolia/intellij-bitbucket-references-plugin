package com.atlassian.bitbucket.linky.rest.server

data class UserBean(
    val displayName: String,
    val slug: String
)
