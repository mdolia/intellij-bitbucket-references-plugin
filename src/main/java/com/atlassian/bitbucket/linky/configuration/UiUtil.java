package com.atlassian.bitbucket.linky.configuration;

import com.intellij.dvcs.repo.Repository;
import com.intellij.openapi.vfs.VfsUtil;
import org.apache.commons.lang.StringUtils;

public class UiUtil {
    private UiUtil() {
        throw new UnsupportedOperationException(getClass().getSimpleName() + " only contains static utility methods " +
                "and should not be instantiated.");
    }

    public static String repositoryRelativePath(Repository repository) {
        String relativePath = VfsUtil.getRelativePath(repository.getRoot(), repository.getProject().getBaseDir());
        if (StringUtils.isBlank(relativePath)) {
            relativePath = "<Project Root>";
        }
        return relativePath;
    }
}
