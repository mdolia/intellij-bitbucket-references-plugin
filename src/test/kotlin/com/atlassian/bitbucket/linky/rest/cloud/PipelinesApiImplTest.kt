package com.atlassian.bitbucket.linky.rest.cloud

import assertk.assertThat
import assertk.assertions.isEqualTo
import com.atlassian.bitbucket.linky.rest.cloud.PipelineStatus.Failed
import com.atlassian.bitbucket.linky.rest.cloud.PipelineStatus.Halted
import com.atlassian.bitbucket.linky.rest.cloud.PipelineStatus.Paused
import com.atlassian.bitbucket.linky.rest.cloud.PipelineStatus.Pending
import com.atlassian.bitbucket.linky.rest.cloud.PipelineStatus.Running
import com.atlassian.bitbucket.linky.rest.cloud.PipelineStatus.Stopped
import com.atlassian.bitbucket.linky.rest.cloud.PipelineStatus.Successful
import com.atlassian.bitbucket.linky.rest.cloud.PipelineTarget.Branch
import com.atlassian.bitbucket.linky.rest.cloud.PipelineTarget.PullRequest
import com.atlassian.bitbucket.linky.rest.cloud.PipelineTarget.Tag
import com.atlassian.bitbucket.linky.rest.cloud.PipelineTrigger.MANUAL
import com.atlassian.bitbucket.linky.rest.cloud.PipelineTrigger.PUSH
import com.atlassian.bitbucket.linky.rest.cloud.PipelineTrigger.SCHEDULE
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.equalTo
import com.github.tomakehurst.wiremock.client.WireMock.get
import com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.okJson
import com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.net.URI
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.util.UUID

@ExtendWith(BitbucketCloudMockExtension::class)
class PipelinesApiImplTest {
    @Test
    fun `BRANCH pipeline in PENDING state triggered MANUALLY`(server: WireMockServer) {
        val items = server.fetchPipelinesPage("pipeline-manual-branch-pending.json")

        assertThat(items).isEqualTo(
            listOf(
                Pipeline(
                    UUID.fromString("33d8d7be-b83b-4630-8218-12e8ca892c05"),
                    Pending,
                    User(
                        UUID.fromString("c28ad334-3dd3-48cc-b19b-ee0427cf3754"),
                        "Daniil Penkin",
                        URI.create("https://avatar-management--avatars.us-west-2.prod.public.atl-paas.net/557057:06df8788-7666-49b6-85cb-5eefc362e243/b168a2b1-8b7f-4bbe-afb2-85da1092cef6/128"),
                    ),
                    buildNumber = 6,
                    ZonedDateTime.of(2021, 9, 12, 12, 21, 1, 691531000, ZoneOffset.UTC.normalized()),
                    completedOn = null,
                    Branch(Commit("5ca2cd5505faca9f934669c77a876561bed651ac", ""), "master"),
                    MANUAL,
                ),
            )
        )
    }

    @Test
    fun `BRANCH pipeline in HALTED state triggered by PUSH`(server: WireMockServer) {
        val items = server.fetchPipelinesPage("pipeline-push-branch-halted.json")

        assertThat(items).isEqualTo(
            listOf(
                Pipeline(
                    UUID.fromString("e5fbf929-3f82-473d-96c7-4eb990089f4d"),
                    Halted,
                    User(
                        UUID.fromString("c28ad334-3dd3-48cc-b19b-ee0427cf3754"),
                        "Daniil Penkin",
                        URI.create("https://avatar-management--avatars.us-west-2.prod.public.atl-paas.net/557057:06df8788-7666-49b6-85cb-5eefc362e243/b168a2b1-8b7f-4bbe-afb2-85da1092cef6/128"),
                    ),
                    buildNumber = 177,
                    ZonedDateTime.of(2021, 9, 12, 15, 44, 26, 546067000, ZoneOffset.UTC.normalized()),
                    completedOn = null,
                    Branch(Commit("f24b8e9afbf397d1d27e86d17733f50aaaf0d532", ""), "master"),
                    PUSH,
                ),
            )
        )
    }

    @Test
    fun `BRANCH pipeline in PAUSED state triggered by PUSH`(server: WireMockServer) {
        val items = server.fetchPipelinesPage("pipeline-push-branch-paused.json")

        assertThat(items).isEqualTo(
            listOf(
                Pipeline(
                    UUID.fromString("d969d68d-58d6-4da8-a731-c26200850de5"),
                    Paused,
                    User(
                        UUID.fromString("c28ad334-3dd3-48cc-b19b-ee0427cf3754"),
                        "Daniil Penkin",
                        URI.create("https://avatar-management--avatars.us-west-2.prod.public.atl-paas.net/557057:06df8788-7666-49b6-85cb-5eefc362e243/b168a2b1-8b7f-4bbe-afb2-85da1092cef6/128"),
                    ),
                    buildNumber = 91,
                    ZonedDateTime.of(2019, 4, 29, 0, 48, 34, 302000000, ZoneOffset.UTC.normalized()),
                    completedOn = null,
                    Branch(
                        Commit(
                            "059b699bbc1375c02eb299af5a0998500c9bd15e",
                            "bitbucket-pipelines.yml edited online with Bitbucket"
                        ),
                        "main",
                    ),
                    PUSH,
                ),
            )
        )
    }

    @Test
    fun `BRANCH pipeline in RUNNING state triggered by SCHEDULE`(server: WireMockServer) {
        val items = server.fetchPipelinesPage("pipeline-schedule-branch-running.json")

        assertThat(items).isEqualTo(
            listOf(
                Pipeline(
                    UUID.fromString("0a0527d3-2aad-4572-8ca8-7a09b2e8275b"),
                    Running,
                    creator = null,
                    buildNumber = 8754,
                    ZonedDateTime.of(2021, 9, 10, 12, 54, 3, 238066000, ZoneOffset.UTC.normalized()),
                    completedOn = null,
                    Branch(
                        Commit(
                            "6bba23105ed6150d0168874ac371b2844ad94f1d",
                            "Merged in dpenkin/NONE-cleanup (pull request #913)\nApproved-by: Vasya Pupkin",
                        ),
                        "main",
                    ),
                    SCHEDULE,
                ),
            )
        )
    }

    @Test
    fun `pipelines in FAILED state are deserialized`(server: WireMockServer) {
        val items = server.fetchPipelinesPage("pipeline-failed.json")

        assertThat(items).isEqualTo(
            listOf(
                Pipeline(
                    UUID.fromString("eed9f85a-a18a-45f6-8aa1-c99bb058d8bd"),
                    Failed(key = null, message = null),
                    creator = null,
                    buildNumber = 8756,
                    ZonedDateTime.of(2021, 9, 10, 18, 54, 1, 518956000, ZoneOffset.UTC.normalized()),
                    ZonedDateTime.of(2021, 9, 10, 19, 43, 41, 128420000, ZoneOffset.UTC.normalized()),
                    Branch(
                        Commit(
                            "6bba23105ed6150d0168874ac371b2844ad94f1d",
                            "Merged in dpenkin/NONE-cleanup (pull request #913)\nApproved-by: Vasya Pupkin",
                        ),
                        "main",
                    ),
                    SCHEDULE,
                ),
                Pipeline(
                    UUID.fromString("29564845-c26a-4d16-8edd-f3f9fcefd7aa"),
                    Failed(
                        "plan-service.parse.detailed-parse-error",
                        "No commands or pipes defined for the step at [pipelines > custom > feature > 0 > step]. Check that you have defined a valid \"script\" section.",
                    ),
                    User(
                        UUID.fromString("c28ad334-3dd3-48cc-b19b-ee0427cf3754"),
                        "Daniil Penkin",
                        URI.create("https://avatar-management--avatars.us-west-2.prod.public.atl-paas.net/557057:06df8788-7666-49b6-85cb-5eefc362e243/b168a2b1-8b7f-4bbe-afb2-85da1092cef6/128"),
                    ),
                    buildNumber = 4,
                    ZonedDateTime.of(2021, 9, 10, 15, 11, 11, 572614000, ZoneOffset.UTC.normalized()),
                    ZonedDateTime.of(2021, 9, 10, 15, 11, 11, 553265000, ZoneOffset.UTC.normalized()),
                    Branch(
                        Commit(
                            "214a2cecef2b0b4493d62eb6c0b21e43718c026d",
                            "bitbucket-pipelines.yml edited online with Bitbucket",
                        ),
                        "master",
                    ),
                    MANUAL,
                ),
            )
        )
    }

    @Test
    fun `BRANCH pipeline in STOPPED state triggered MANUALLY`(server: WireMockServer) {
        val items = server.fetchPipelinesPage("pipeline-manual-branch-stopped.json")

        assertThat(items).isEqualTo(
            listOf(
                Pipeline(
                    UUID.fromString("a43fa277-8c0d-4a45-851c-c924a140625a"),
                    Stopped(
                        User(
                            UUID.fromString("c28ad334-3dd3-48cc-b19b-ee0427cf3754"),
                            "Daniil Penkin",
                            URI.create("https://avatar-management--avatars.us-west-2.prod.public.atl-paas.net/557057:06df8788-7666-49b6-85cb-5eefc362e243/b168a2b1-8b7f-4bbe-afb2-85da1092cef6/128"),
                        )
                    ),
                    User(
                        UUID.fromString("c28ad334-3dd3-48cc-b19b-ee0427cf3754"),
                        "Daniil Penkin",
                        URI.create("https://avatar-management--avatars.us-west-2.prod.public.atl-paas.net/557057:06df8788-7666-49b6-85cb-5eefc362e243/b168a2b1-8b7f-4bbe-afb2-85da1092cef6/128"),
                    ),
                    buildNumber = 8682,
                    ZonedDateTime.of(2021, 9, 2, 14, 35, 57, 175016000, ZoneOffset.UTC.normalized()),
                    ZonedDateTime.of(2021, 9, 2, 16, 14, 17, 222263000, ZoneOffset.UTC.normalized()),
                    Branch(
                        Commit(
                            "fe926ea267d46d2d508652a5e30177c18e5190df",
                            "Merged in dpenkin/NONE-do-not-delete-some-projects (pull request #910)\nApproved-by: Vasya Pupkin",
                        ),
                        "main",
                    ),
                    MANUAL,
                ),
            )
        )
    }

    @Test
    fun `PULL REQUEST pipeline in SUCCESSFUL state triggered by PUSH`(server: WireMockServer) {
        val items = server.fetchPipelinesPage("pipeline-push-pullrequest-successful.json")

        assertThat(items).isEqualTo(
            listOf(
                Pipeline(
                    UUID.fromString("0a95a064-73f4-4eff-af0e-2630dff37457"),
                    Successful,
                    User(
                        UUID.fromString("c28ad334-3dd3-48cc-b19b-ee0427cf3754"),
                        "Daniil Penkin",
                        URI.create("https://avatar-management--avatars.us-west-2.prod.public.atl-paas.net/557057:06df8788-7666-49b6-85cb-5eefc362e243/b168a2b1-8b7f-4bbe-afb2-85da1092cef6/128"),
                    ),
                    buildNumber = 87,
                    ZonedDateTime.of(2019, 3, 18, 4, 58, 31, 615000000, ZoneOffset.UTC.normalized()),
                    ZonedDateTime.of(2019, 3, 18, 4, 58, 45, 722000000, ZoneOffset.UTC.normalized()),
                    PullRequest(
                        Commit(
                            "b36cf8b5b2b3e0a6bb40ab40c208c5d556066b8a",
                            "[skip ci] testing",
                        ),
                        prId = 8,
                        "[skip ci] testing PR",
                        "dpenkin/skip-ci-testing-1552885108284",
                        "master",
                    ),
                    PUSH,
                ),
            )
        )
    }

    @Test
    fun `TAG pipeline in SUCCESSFUL state triggered by PUSH`(server: WireMockServer) {
        val items = server.fetchPipelinesPage("pipeline-push-tag-successful.json")

        assertThat(items).isEqualTo(
            listOf(
                Pipeline(
                    UUID.fromString("f511b06b-03f6-4433-9137-3a15e7ea67cc"),
                    Successful,
                    User(
                        UUID.fromString("c28ad334-3dd3-48cc-b19b-ee0427cf3754"),
                        "Daniil Penkin",
                        URI.create("https://avatar-management--avatars.us-west-2.prod.public.atl-paas.net/557057:06df8788-7666-49b6-85cb-5eefc362e243/b168a2b1-8b7f-4bbe-afb2-85da1092cef6/128"),
                    ),
                    buildNumber = 70,
                    ZonedDateTime.of(2018, 10, 8, 3, 47, 53, 550000000, ZoneOffset.UTC.normalized()),
                    ZonedDateTime.of(2018, 10, 8, 3, 48, 52, 570000000, ZoneOffset.UTC.normalized()),
                    Tag(
                        Commit(
                            "187bcf15cfac16dfe71f2097a255b21d12689376",
                            "Merged in version-bump (pull request #7)",
                        ),
                        "v0.6.1",
                    ),
                    PUSH,
                ),
            )
        )
    }

    private fun WireMockServer.fetchPipelinesPage(filename: String): List<Pipeline> {
        val repositoryId = uniqueRepositoryId()
        stubFor(
            get(urlPathEqualTo(repositoryId.pipelinesPageUrlPath()))
                .willReturn(okJson(readResourceFile(filename)))
        )

        val pipelines = BitbucketCloudApiImpl()
            .repository(repositoryId)
            .pipelines()
            .page()
            .get()
            .items

        verify(
            getRequestedFor(urlPathEqualTo(repositoryId.pipelinesPageUrlPath()))
                .withQueryParam("pagelen", equalTo("10"))
                .withQueryParam("page", equalTo("1"))
                .withQueryParam("fields", equalTo("+values.target.commit.message"))
                .withQueryParam("sort", equalTo("-created_on"))
        )

        return pipelines
    }

    private fun RepositoryId.pipelinesPageUrlPath(): String = "/2.0/repositories/$workspace/$slug/pipelines/"
}
