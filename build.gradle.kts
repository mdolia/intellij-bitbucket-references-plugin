import org.apache.tools.ant.filters.ReplaceTokens

plugins {
    id("org.jetbrains.intellij") version "1.1.6"
    kotlin("jvm") version "1.5.31"
}

val intellijBuild: String by project

group = "com.atlassian.bitbucket.pipelines"
version = intellijBuild

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("stdlib-common"))

    val commonmarkVersion = "0.18.0"
    val fuelVersion = "2.3.1"
    val nanoHttpdVersion = "2.3.1"

    implementation("org.commonmark", "commonmark", commonmarkVersion)
    implementation("org.nanohttpd", "nanohttpd", nanoHttpdVersion)
    implementation("com.github.kittinunf.fuel", "fuel", fuelVersion)
    implementation("com.github.kittinunf.fuel", "fuel-gson", fuelVersion)

    val assertkVersion = "0.24"
    val jsonassertVersion = "1.5.0"
    val junitVersion = "5.7.2"
    val mockkVersion = "1.12.0"
    val slf4jVersion = "1.7.32"
    val wiremockVersion = "2.27.2"

    testImplementation("io.mockk", "mockk", mockkVersion)
    testImplementation("com.willowtreeapps.assertk", "assertk-jvm", assertkVersion)
    testImplementation("org.skyscreamer", "jsonassert", jsonassertVersion) {
        exclude("junit", "junit")
    }
    testImplementation("org.junit.jupiter", "junit-jupiter-api", junitVersion)
    testImplementation("org.junit.jupiter", "junit-jupiter-params", junitVersion)
    testRuntimeOnly("org.junit.jupiter", "junit-jupiter-engine", junitVersion)
    testImplementation("com.github.tomakehurst", "wiremock", wiremockVersion) {
        exclude("junit", "junit")
    }
    testRuntimeOnly("org.slf4j", "slf4j-simple", slf4jVersion)
}

val releaseChannel = System.getenv("BITBUCKET_DEPLOYMENT_ENVIRONMENT")?.substringBefore('-') ?: "local"
val prodRelease = "production".equals(releaseChannel, ignoreCase = true)

intellij {
    version.set(intellijBuild)
    plugins.set(listOf("yaml", "sh", "git4idea"))

    // Only non-production releases are open-ended
    sameSinceUntilBuild.set(prodRelease.not())
}

kotlin {
    target {
        compilations.all {
            kotlinOptions {
                allWarningsAsErrors = true
                jvmTarget = "11"
            }
        }
    }
}

tasks {
    patchPluginXml {
        val buildNumber = System.getenv("BITBUCKET_BUILD_NUMBER") ?: "local"
        val buildSuffix = when (releaseChannel) {
            "production" -> ""
            "eap" -> ".eap"
            else -> ".${releaseChannel.substring(0, 1)}"
        }
        version.set("${project.version}.$buildNumber$buildSuffix")

        changeNotes.set(
            """
            <h5>Changed</h5>
            <ul>
              <li>Drop support for Mercurial repositories which are not supported in Bitbucket anymore</li>
            </ul>
            """.trimIndent()
        )
    }

    publishPlugin {
        token.set(System.getenv("JB_API_TOKEN"))
        if (prodRelease.not()) {
            channels.set(listOf(releaseChannel))
        }
    }

    test {
        useJUnitPlatform()
        testLogging {
            events("passed", "skipped", "failed")
        }
    }

    processResources {
        from("src/main/resources/schemas") {
            rename("([^.]+)(.+)", "$1-internal$2")
            filter(ReplaceTokens::class, "tokens" to mapOf("supported_sizes" to """1x", "2x", "4x"""))
            into("schemas")
        }

        from("src/main/resources/schemas") {
            filter(ReplaceTokens::class, "tokens" to mapOf("supported_sizes" to """1x", "2x"""))
            into("schemas")
            duplicatesStrategy = DuplicatesStrategy.INCLUDE
        }
    }
}
